FROM python
# Define a base image for your application. Choose the appropriate image based on your application's requirements.
#RUN apt  install python3-pip -y
WORKDIR /app
RUN apt update

RUN pip install streamlit_webrtc
RUN pip install keras.models
RUN pip install numpy
RUN pip install av
# Set a working directory for your application code
 
# Copy your application code from the local directory into the container
COPY . .
 
# Expose the port your application listens on
EXPOSE 80
# Specify the command to run when the container starts (replace with your actual application entrypoint)
# CMD [ "python3", "-m" , "flask" , "run", "--host=0.0.0.0"]
CMD [ "python3", "music.py"]
